<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QTranslationSwitch</name>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="91"/>
        <source>System</source>
        <translation type="unfinished">Системный</translation>
    </message>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="92"/>
        <source>Set system language</source>
        <translation type="unfinished">Установить системный язык</translation>
    </message>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="93"/>
        <source>Russian</source>
        <translation type="unfinished">Русский</translation>
    </message>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="94"/>
        <source>Set russian language</source>
        <translation type="unfinished">Установить русский язык</translation>
    </message>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="95"/>
        <source>English</source>
        <translation type="unfinished">Английский</translation>
    </message>
    <message>
        <location filename="../QTranslationSwitch.cpp" line="96"/>
        <source>Set english language</source>
        <translation type="unfinished">Установить английский язык</translation>
    </message>
</context>
<context>
    <name>QtTranslationSwitch</name>
    <message>
        <source>System</source>
        <translation type="vanished">Системный</translation>
    </message>
    <message>
        <source>Set system language</source>
        <translation type="vanished">Установить системный язык</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Русский</translation>
    </message>
    <message>
        <source>Set russian language</source>
        <translation type="vanished">Установить русский язык</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Английский</translation>
    </message>
    <message>
        <source>Set english language</source>
        <translation type="vanished">Установить английский язык</translation>
    </message>
</context>
</TS>
