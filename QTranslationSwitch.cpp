#include "QTranslationSwitch.h"
#ifndef QTRANSLATIONSWITCH_NO_GUI
#include <QActionGroup>
#include <QAction>
#include <QMenu>
#include <QIcon>
#include <QApplication>
#else
#include <QCoreApplication>
#endif
#include <QTranslator>

#include <QLibraryInfo>
#include <QLocale>
#include <QDir>
#include <QStringBuilder>

QTranslationSwitch::QTranslationSwitch() {
#ifndef QTRANSLATIONSWITCH_NO_GUI
    m_actGroup = new QActionGroup{this};
    connect(m_actGroup, &QActionGroup::triggered, this,
            &QTranslationSwitch::onLanguageChanged);

    m_actLangSys = new QAction{this};
    m_actLangSys->setCheckable(true);
    m_actLangSys->setData(langNameSys());
    m_actGroup->addAction(m_actLangSys);

    m_actLangRu = new QAction{this};
    m_actLangRu->setData(langNameRu());
    m_actLangRu->setIcon(QIcon(QStringLiteral(":/qtranslationswitch/lang/ru.png")));
    m_actLangRu->setCheckable(true);
    m_actGroup->addAction(m_actLangRu);

    m_actLangEng = new QAction{this};
    m_actLangEng->setData(langNameEng());
    m_actLangEng->setIcon(QIcon(QStringLiteral(":/qtranslationswitch/lang/en.png")));
    m_actLangEng->setCheckable(true);
    m_actGroup->addAction(m_actLangEng);
#endif
}

QTranslationSwitch::~QTranslationSwitch() {
    qDeleteAll(m_translators);
}

QTranslationSwitch &QTranslationSwitch::instance() {
    static QTranslationSwitch sw;
    return sw;
}

void QTranslationSwitch::init(const QString &prj_translation_dir) {
    m_prjTransDir = QDir::isRelativePath(prj_translation_dir) ?
        (qApp->applicationDirPath() % "/" % prj_translation_dir) :
        prj_translation_dir;

    addTranslationFile(QStringLiteral("qt"),
         #ifdef Q_OS_WIN
             QString{}
         #else
             QLibraryInfo::location(QLibraryInfo::TranslationsPath)
         #endif
            );
    addTranslationFile(QStringLiteral("qtranslationswitch"));
}

void QTranslationSwitch::addTranslationFile(const QString &filename, const QString &dir) {
    m_translators.append(new TranslatorInfo(filename,
#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
                                            dir.isEmpty()
#else
                                            (dir.count() == 0)
#endif
                                                ? m_prjTransDir : dir));
}

#ifndef QTRANSLATIONSWITCH_NO_GUI
QList<QAction *> QTranslationSwitch::actions() const {
    return m_actGroup->actions();
}
#endif

QLocale QTranslationSwitch::currentLocale() const {
    return m_curLangName.isEmpty() ? QLocale::system() : QLocale(m_curLangName);
}

#ifndef QTRANSLATIONSWITCH_NO_GUI
void QTranslationSwitch::onLanguageChanged(QAction *act) {
    const QString locName {act->data().toString()};
    if (locName != m_curLangName)
        setLanguage(locName);
}
#endif

void QTranslationSwitch::retranslate() {
#ifndef QTRANSLATIONSWITCH_NO_GUI
    m_actLangSys->setText(tr("System"));
    m_actLangSys->setToolTip(tr("Set system language"));
    m_actLangRu->setText(tr("Russian"));
    m_actLangRu->setToolTip(tr("Set russian language"));
    m_actLangEng->setText(tr("English"));
    m_actLangEng->setToolTip(tr("Set english language"));
#endif
}

void QTranslationSwitch::setLanguage(const QString &name) {
    m_curLangName = name;

    const QLocale loc {currentLocale()};
#ifndef QTRANSLATIONSWITCH_NO_GUI
    const QList<QAction *> &actions {m_actGroup->actions()};
    for (QAction *act : actions) {
        if (act->data().toString() == name) {
            act->setChecked(true);
        }
    }
#endif

    const QList<TranslatorInfo *> &translators {m_translators};
    for (TranslatorInfo *translator : translators) {
        translator->reload(loc);
    }
    retranslate();
}


QTranslationSwitch::TranslatorInfo::TranslatorInfo(
        const QString &init_file, const QString &init_dir)  :
    m_translator{new QTranslator{}},
    m_filename{init_file},
    m_dir{init_dir} {

}

QTranslationSwitch::TranslatorInfo::~TranslatorInfo() {
    delete m_translator;
}

void QTranslationSwitch::TranslatorInfo::reload(const QLocale &locale) {
    qApp->removeTranslator(m_translator);

#if (QT_VERSION >= QT_VERSION_CHECK(4, 8, 0))
    m_translator->load(locale, m_filename, QStringLiteral("_"), m_dir);
#else
    m_translator->load(m_filename + "_" + locale.name(), m_dir);
#endif
    qApp->installTranslator(m_translator);
}
