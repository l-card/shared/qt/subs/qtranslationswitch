#ifndef  QTRANSLATIONSWITCH_H
#define QTRANSLATIONSWITCH_H

#include <QObject>
#include <QHash>
#include <QString>


class QMenu;
#ifndef QTRANSLATIONSWITCH_NO_GUI
class QAction;
class QActionGroup;
#endif
class QTranslator;
class QLocale;

/* Класс для упрощенной реализации переключения языков. Необходимо
   создать при старте с указанием директории по умолчаню с qm файлами.
   Класс создает группу действий для переключения языков, которые можно
   получить через метод actions() (чтобы, например, добавить в меню).
   Если в приложении есть в ресурсах :/icons/ru.png и :/icons/en.png, то они
   используются для действий установки соотвествтующих языков.

   Перед работой необходимо для каждого qm файла вызвать addTranslationFile c
   указанием базого имени файла (без _<язык>.qm).

   После чего, необходимо установить начальный язык, вызвав setLanguage().
   После этого класс сам обслуживает смену языка по действиям.

   При завершении работы можно получить текущее имя языка через currentLanguageName(),
   чтобы сохранить в настройках.
*/

class QTranslationSwitch : public QObject {
    Q_OBJECT
public:
    ~QTranslationSwitch();

    static QTranslationSwitch &instance();

    void init(const QString &prj_translation_dir);
    void addTranslationFile(const QString &filename, const QString &dir = QString());

#ifndef QTRANSLATIONSWITCH_NO_GUI
    QList<QAction *> actions() const;
#endif
    QString currentLanguageName() const {return m_curLangName;}
    QLocale currentLocale() const;

    static const QString langNameRu()  {return QStringLiteral("ru");}
    static const QString langNameEng() {return QStringLiteral("en");}
    static const QString langNameSys() {return QString();}
public Q_SLOTS:
    void setLanguage(const QString &name);
private Q_SLOTS:
#ifndef QTRANSLATIONSWITCH_NO_GUI
    void onLanguageChanged(QAction *act);
#endif
private:
    QTranslationSwitch();

    void retranslate();

    struct TranslatorInfo {
        TranslatorInfo(const QString &init_file, const QString &init_dir);
        ~TranslatorInfo();

        void reload(const QLocale &locale);
    private:
        QTranslator *m_translator;
        QString m_filename;
        QString m_dir;
    };

    QString m_curLangName;
    QString m_prjTransDir;
#ifndef QTRANSLATIONSWITCH_NO_GUI
    QActionGroup *m_actGroup;
    QAction *m_actLangSys, *m_actLangRu, *m_actLangEng;
#endif
    QList<TranslatorInfo *> m_translators;
};

#endif // QTRANSLATIONSWITCH_H
